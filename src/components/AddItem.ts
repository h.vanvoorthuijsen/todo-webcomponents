import { html, css, LitElement } from 'lit';
import { query, property } from 'lit/decorators.js';
import { ScopedElementsMixin } from '@open-wc/scoped-elements/lit-element.js';
import { MyButtonSubmit } from './elements/MyButton.js';
import MyInput from './elements/MyInput.js';

class AddItem extends ScopedElementsMixin(LitElement) {
  static scopedElements = {
    'my-button-submit': MyButtonSubmit,
    'my-input': MyInput,
  };
  static styles = css`
    :host {
      display: block;
      border-top: 1px solid var(--border-color);
      padding-inline: 25px;
      padding-block-start: 25px;
    }

    my-input {
      margin-block-end: 12px;
    }

    my-button-submit {
      background-color: var(--color-primary);
    }

    my-button-submit:hover {
      background-color: var(--color-primary-lighter);
    }
  `;

  @query('my-input input')
  _input!: HTMLInputElement;

  @property()
  value = '';

  __handleSubmit = (event: Event) => {
    event.preventDefault();
    const value = this._input.value.trim();
    if (value) {
      this.dispatchEvent(
        new CustomEvent<TodoItemType>('item-added', {
          detail: { value, createdAt: new Date() },
        })
      );
      setTimeout(() => {
        this._input.value = '';
      }, 0);
    }
  };

  render() {
    return html`<form @submit="${this.__handleSubmit}">
      <my-input label="Todo item text: "></my-input
      ><my-button-submit >Add todo item</my-button>
    </form>`;
  }
}

export default AddItem;
