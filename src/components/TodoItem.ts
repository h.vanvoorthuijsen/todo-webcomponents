import { html, css, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { ScopedElementsMixin } from '@open-wc/scoped-elements/lit-element.js';
import MyCheckbox from './elements/MyCheckbox';

class TodoItem extends ScopedElementsMixin(LitElement) {
  static scopedElements = {
    'my-checkbox': MyCheckbox,
  };
  static styles = css`
    my-checkbox {
      display: flex;
      gap: 12px;
      align-items: center;
      cursor: pointer;
    }
  `;

  @query('my-checkbox input')
  _input!: HTMLInputElement;

  @property({ type: Object })
  item: TodoItemType | null = null;

  __handleClicked = (event: Event) => {
    event.preventDefault();
    event.cancelBubble = true;
    if (!this.item) {
      return;
    }
    this.dispatchEvent(
      new CustomEvent<TodoItemType>('item-changed', {
        detail: {
          ...this.item,
          doneAt: this.item?.doneAt ? undefined : new Date(),
        },
      })
    );
  };

  render() {
    if (!this.item) {
      return null;
    }
    return html`${this.item.doneAt
        ? html`<style>
            :host {
              opacity: 0.5;
            }
          </style>`
        : null}
      <my-checkbox
        .checked="${!!this.item.doneAt}"
        @click="${this.__handleClicked}"
        @change="${this.__handleClicked}"
        ><div slot="label">${this.item.value}</div></my-checkbox
      >`;
  }
}

export default TodoItem;
