import { html, css, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ScopedElementsMixin } from '@open-wc/scoped-elements/lit-element.js';
import TodoItem from './TodoItem';

class TodoList extends ScopedElementsMixin(LitElement) {
  static scopedElements = {
    'todo-item': TodoItem,
  };
  static styles = css`
    ul {
      list-style: none;
      margin: 0;
      padding: 0;
      display: flex;
      flex-direction: column;
      gap: 12px;
      margin-block-end: 16px;
      padding-inline: 25px;
    }
    .nothing {
      padding-inline: 25px;
      margin-block-end: 16px;
      color: gray;
    }
  `;

  @property({ type: Array })
  items: TodoItemType[] = [];

  @property()
  hideDone = false;

  __handleItemChanged =
    (index: number) => (event: CustomEvent<TodoItemType>) => {
      this.dispatchEvent(
        new CustomEvent<{ item: TodoItemType; index: number }>('item-changed', {
          detail: {
            item: event.detail,
            index,
          },
        })
      );
    };

  render() {
    const items = this.items
      .map((item, index) => ({ ...item, index }))
      .filter(item => !this.hideDone || !item.doneAt);

    if (!items.length) {
      return html`<div class="nothing">
        Nothing to do, create a new item below
      </div>`;
    }

    return html`
      <ul>
        ${items.map(
          ({ index, ...item }) =>
            html`<li>
              <todo-item
                .item="${item}"
                @item-changed="${this.__handleItemChanged(index)}"
              ></todo-item>
            </li>`
        )}
      </ul>
    `;
  }
}

export default TodoList;
