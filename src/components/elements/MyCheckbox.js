import { css, html, LitElement } from 'lit';
import { ScopedElementsMixin } from '@open-wc/scoped-elements/lit-element.js';
import { LionCheckbox } from '@lion/ui/checkbox-group.js';
import { property } from 'lit/decorators.js';

class MyCheckbox extends ScopedElementsMixin(LionCheckbox) {
  static get styles() {
    return [
      ...super.styles,
      css`
        ::slotted(input) {
          appearance: none;
          color: currentColor;
          width: 1.15em;
          height: 1.15em;
          border: 1px solid var(--border-color);
          border-radius: var(--border-radius);
          display: flex;
          justify-content: center;
          align-items: center;
          font-size: 20px;
        }

        ::slotted(input)::before {
          display: block;
          content: '';
          width: 0.9em;
          height: 0.9em;

          clip-path: polygon(
            14% 44%,
            0 65%,
            50% 100%,
            100% 16%,
            80% 0%,
            43% 62%
          );
          transform: scale(0);
          transform-origin: bottom left;
          transition: 120ms transform ease-in-out;
          box-shadow: inset 1em 1em var(--color-primary);
          /* Windows High Contrast Mode */
        }

        :host([checked]) ::slotted(input)::before {
          transform: scale(1);
        }

        ::slotted(input):focus {
          outline: 1px solid currentColor;
          outline-offset: 0;
        }
      `,
    ];
  }
}

export default MyCheckbox;
