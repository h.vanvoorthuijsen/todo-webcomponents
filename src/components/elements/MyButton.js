import { LionButton, LionButtonSubmit } from '@lion/ui/button.js';
import { css } from 'lit';

export const buttonStyles = css`
  :host {
    cursor: pointer;
    border-radius: var(--border-radius);
  }
`;

export class MyButtonSubmit extends LionButtonSubmit {
  static get styles() {
    return [...super.styles, buttonStyles];
  }
}

class MyButton extends LionButton {
  static get styles() {
    return [...super.styles, buttonStyles];
  }
  static Submit = MyButtonSubmit;
}

export default MyButton;
