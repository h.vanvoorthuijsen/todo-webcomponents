import { LionInput } from '@lion/ui/input.js';
import { css } from 'lit';

class MyInput extends LionInput {
  static get styles() {
    return [
      ...super.styles,
      css`
        ::slotted(input) {
          border: 1px solid var(--border-color);
          border-radius: var(--border-radius);
          padding: 8px;
        }
        ::slotted(label) {
          display: block;
          margin-block-end: 4px;
        }
      `,
    ];
  }
}

export default MyInput;
