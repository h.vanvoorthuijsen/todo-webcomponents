type TodoItemType = {
  createdAt: Date;
  value: string;
  doneAt?: Date;
};
