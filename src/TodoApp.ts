import { html, css, LitElement, PropertyValueMap } from 'lit';
import { property, customElement } from 'lit/decorators.js';
import { ScopedElementsMixin } from '@open-wc/scoped-elements/lit-element.js';
import './components/TodoList';
import AddItem from './components/AddItem';
import TodoList from './components/TodoList';
import MyButton from './components/elements/MyButton';
import todoItemSorter from './helpers/todoItemSorter';

@customElement('todo-app')
export class TodoApp extends ScopedElementsMixin(LitElement) {
  static scopedElements = {
    'add-item': AddItem,
    'todo-list': TodoList,
    'my-button': MyButton,
  };
  static styles = css`
    :host {
      --border-radius: 4px;
      --color-primary: #ffba00;
      --color-primary-darker: #f0b000;
      --color-primary-lighter: #ffcc3e;
      --border-color: gray;

      font-family: Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
      display: block;
      color: var(--todo-app-text-color, #000);
      max-width: 600px;
      margin: 0 auto;
      border: 1px solid var(--border-color);
      border-radius: 12px;
      display: flex;
      flex-direction: column;
      overflow: hidden;
      position: relative;
      background: white;
    }

    .header {
      padding: 12px 24px;
      background: var(--color-primary);
    }

    .header h1 {
      margin: 0;
    }

    .content {
      padding-block: 25px;
    }

    .material-symbols-outlined {
      font-family: 'Material Symbols Outlined';
      font-weight: normal;
      font-style: normal;
      font-size: 24px;
      line-height: 1;
      letter-spacing: normal;
      text-transform: none;
      display: inline-block;
      white-space: nowrap;
      word-wrap: normal;
      direction: ltr;
      -webkit-font-feature-settings: 'liga';
      -webkit-font-smoothing: antialiased;
    }

    .buttons {
      position: absolute;
      right: 24px;
      top: 12px;
    }
  `;

  connectedCallback() {
    super.connectedCallback();
    this.items = this.__getFromStorage();
  }

  @property()
  header = 'Things to do';

  @property()
  localStorageKey = 'todo-app-items';

  @property({ type: Array })
  items: TodoItemType[] = [];

  @property()
  hideDone = false;

  protected updated(
    changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>
  ): void {
    if (changedProperties.has('items')) {
      window.localStorage.setItem(
        this.localStorageKey,
        JSON.stringify(this.items)
      );
    }
  }

  __getFromStorage = () => {
    const data = window.localStorage.getItem(this.localStorageKey);
    if (!data) {
      return [];
    }
    const items = JSON.parse(data, (key, value) => {
      if (['createdAt', 'doneAt'].includes(key)) {
        return new Date(value);
      }
      return value;
    });
    return items;
  };

  __handleItemAdded = (event: CustomEvent<TodoItemType>) => {
    this.items = [...this.items, event.detail];
  };

  __handleItemChanged = ({
    detail: { item, index },
  }: CustomEvent<{ item: TodoItemType; index: number }>) => {
    const newItems = [...this.items];
    newItems.splice(index, 1, item);
    this.items = newItems;
  };

  __handleItemSort = () => {
    this.items = [...this.items].sort(todoItemSorter);
  };

  __handleDeleteItems = () => {
    this.items = this.items.filter(item => !item.doneAt);
  };

  render() {
    return html` <div class="header">
        <h1>${this.header}</h1>
        <div class="buttons">
          ${this.hideDone !== false
            ? undefined
            : html`<my-button
                @click="${this.__handleDeleteItems}"
                title="Delete items that are done"
                ><span class="material-symbols-outlined"
                  >&#xe872;</span
                ></my-button
              >`}
          <my-button @click="${this.__handleItemSort}" title="Sort"
            ><span class="material-symbols-outlined">&#xe8d5;</span></my-button
          >
        </div>
      </div>
      <div class="content">
        <todo-list
          .items="${this.items}"
          @item-changed="${this.__handleItemChanged}"
          .hideDone="${this.hideDone !== false}"
        ></todo-list>
        <add-item @item-added="${this.__handleItemAdded}"></add-item>
      </div>`;
  }
}
