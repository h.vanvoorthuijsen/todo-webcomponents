import todoItemSorter from './todoItemSorter';

describe('todoItemSorter', () => {
  const cases: {
    items: TodoItemType[];
    expectedOrder: string[];
    toString: () => string;
  }[] = [
    {
      items: [
        { value: 'A', createdAt: new Date('2024-01-03') },
        { value: 'C', createdAt: new Date('2024-01-01') },
        { value: 'B', createdAt: new Date('2024-01-02') },
      ],
      expectedOrder: ['A', 'B', 'C'],
      toString: () => 'Sort on dates',
    },
    {
      items: [
        { value: 'B', createdAt: new Date('2024-01-03'), doneAt: new Date() },
        { value: 'A', createdAt: new Date('2024-01-01') },
      ],
      expectedOrder: ['A', 'B'],
      toString: () => 'Sort state',
    },
    {
      items: [
        {
          value: 'A',
          createdAt: new Date('2024-01-03'),
          doneAt: new Date('2024-01-03'),
        },
        {
          value: 'C',
          createdAt: new Date('2024-01-01'),
          doneAt: new Date('2024-01-01'),
        },
        {
          value: 'B',
          createdAt: new Date('2024-01-01'),
          doneAt: new Date('2024-01-02'),
        },
      ],
      expectedOrder: ['A', 'B', 'C'],
      toString: () => 'Sort state on date',
    },
    {
      items: [
        {
          value: 'C',
          createdAt: new Date('2024-01-03'),
          doneAt: new Date('2024-02-02'),
        },
        { value: 'B', createdAt: new Date('2024-01-01') },
        {
          value: 'D',
          createdAt: new Date('2024-01-02'),
          doneAt: new Date('2024-02-01'),
        },
        { value: 'A', createdAt: new Date('2024-01-04') },
      ],
      expectedOrder: ['A', 'B', 'C', 'D'],
      toString: () => 'Sort both state and date',
    },
  ];

  test.each(cases)('%s', ({ items, expectedOrder }) => {
    const list = [...items];
    list.sort(todoItemSorter);
    expect(list.map(({ value }) => value)).toEqual(expectedOrder);
  });
});
