const todoItemSorter = (a: TodoItemType, b: TodoItemType) => {
  if (a.doneAt && !b.doneAt) {
    return 1;
  }
  if (b.doneAt && !a.doneAt) {
    return -1;
  }
  if (a.doneAt && b.doneAt) {
    return b.doneAt.getTime() - a.doneAt.getTime();
  }
  return b.createdAt.getTime() - a.createdAt.getTime();
};

export default todoItemSorter;
